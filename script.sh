#!/bin/bash
validateCommand() {
  if [ $? -eq 0 ]; then
    echo "[OK] $1"
  else
    echo "[ERROR] $1"
    exit 1
  fi
}

# Instalando Docker
cd /tmp/ >/dev/null 2>>/dev/null
curl -fsSL https://get.docker.com -o get-docker.sh >/dev/null 2>>/dev/null
sudo sh ./get-docker.sh >/dev/null 2>>/dev/null
validateCommand "Docker Instalado!" 

# Configurando Clair
cd ~ >/dev/null 2>>/dev/null
git clone https://gitlab.com/o_sgoncalves/clair-locallab.git >/dev/null 2>>/dev/null
cd ~/clair-locallab/ >/dev/null 2>>/dev/null
docker compose up -d >/dev/null 2>>/dev/null
validateCommand "Clair Configurado!"